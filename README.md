# README #

** By the team at **

https://www.thebold.nz/

---

# Bleeding edge development

A lot of the time, budget restricts what can be done to a website. When you start a project with us, we tell you that you can spend a little or a lot on a website. A little will get you a conservative approach, and a lot will get you the bleeding edge approach.

While both ends of the spectrum will see you getting a premium quality website, designed to last, it is the bleeding edge end that will really set you apart from the crowd. The term bleeding edge means we use techniques that are so new, that only the latest and best browsers will correctly render them. Older browsers will gracefully fall back to older methods.

## CSS 3

CSS is what makes a site look they way it does. Without CSS, browsers would render everything in plain black Times New Roman text on a white background. CSS 3 is the latest specification, and it has a lot of awesome features. Most of them have become standard, with major browsers supporting them. But some are still too new, like text colour gradients. 

### Text gradients

Check out the intro text on our [about page](https://www.thebold.nz/about/), this awesome feature is currently only available in Chrome and Safari, and it allows us to add a colour gradient to text. In other browsers, we can make it fall back to a solid colour, but this is a great example of why using [Google Chrome](https://www.google.com/chrome/) as your main browser is recommended. Small details like this really adds a point of difference.

### Blending mode

Another CSS 3 technique is called Blending Mode. This allows us to manipulate images in the browser, to create visually dynamic effects. On our [homepage](https://www.thebold.nz/), we are using the blending mode Multiply on the clouds video to seamlessly blend with the blue background and make it seem as one. Some other great examples are:

- To show and hide specific text [http://bolden.nl/](http://bolden.nl/)
- Duotone technique like Spotify have used recently [see example](http://codepen.io/72lions/full/jPzLJX/)

### 3d rendering

While we could use Canvas to create very complicated 3d rendering, we can also use CSS 3. Like on a graph, elements on a web page exist on a plane. Because web pages are 2 dimensional, those planes normally only include a `X` and `Y` axis. But we can also make use of the `Z` axis. This creates depth and the illusion of being 3 dimensional. A great example is our [404 page](/404/), where we are using CSS 3 translates to give the block building world depth and the ability to rotate on the `X`, `Y`, and `Z` axis. This, paired with Javascript, means we can create interactive 3 dimensional elements within a website.

### Flexbox

Believe it or not, arranging elements on a webpage can be very difficult. The more complicated the grid, the more difficult it is to correctly position these elements, even more so when it has to act responsively on multiple browser sizes and device screens. Something as seemingly simple as placing text vertically and horizontally centered within a container can be quite difficult. That was, at least, until Flexbox was released.

Using the above example, prior to Flexbox the best technique to center an element was to position it absolutely with left and top offsets, then translate it back on the `X` and `Y` axis the same value as the offset but in negative. This sounds complicated, and for something so common, it definitely is overly complicated to implement. Flexbox has made it much easier to do. It gives us the power to directly change the direction, both vertically and horizontally, of any element and keep it in relation to it's sibling elements. We can center elements using flexbox, reverse the order of elements, collapse rows of elements gracefully and maintain a site's grid.

Our website is built using the Flexbox model, the best example of which is the [home page](/).

## Canvas

The [HTML5 Canvas API](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API) uses the html canvas element and the scripting language Javascript. With these two tools, we can create complicated renderings. On our site, we created a game of Noughts and Crosses on the [services page](/services/), but it can be used for almost any 2d or 3d rendering. Graphs, games, image manipulation, video rendering, and animations. Some  great examples are:

- 3d rendering [see examples](https://www.chromeexperiments.com/not-webgl?q=canvas&page=0)
- Charts/graphs [see examples](http://canvasjs.com/html5-javascript-line-chart/)
- Games [see example](http://www.playkeepout.com/)

## Not bleeding edge, but underutilised

We have also used techniques that aren't bleeding edge, but they are definitely not used enough. On our [contact page](https://www.thebold.nz/contact/), we have made use of an image sprite to create an interactive button animation. Hover over the the bird cage and the bird will jump across. Successfully send a form enquiry, and the bird will escape from the cage and fly away (watch for the little present it leaves behind).

The form also uses AJAX to dynamically send the form and retrieve a response, so the whole process remains seamless.

SVGs (scalable vector graphics) are used site wide for most icons and symbols, instead of images. While these aren't a new technology, they aren't used enough. They help reduce page load time and create crystal clear lines, which is very important when building responsive websites.